name := "aoc2021-scala"

version := "0.1"

scalaVersion := "2.13.7"

scalacOptions := Seq("-unchecked", "-deprecation")

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.10" % "test"
