package jessewebb.aoc2021.day01

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Part2Test extends AnyFunSuite with Matchers {

  test("Example") {
    val measurements = List(
      199,
      200,
      208,
      210,
      200,
      207,
      240,
      269,
      260,
      263,
    )
    val answer = Part2.solve(measurements)
    answer shouldEqual 5
  }

}
