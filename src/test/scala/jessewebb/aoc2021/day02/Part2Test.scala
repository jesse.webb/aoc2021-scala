package jessewebb.aoc2021.day02

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Part2Test extends AnyFunSuite with Matchers {

  test("Example") {
    val commands = List(
      "forward 5",
      "down 5",
      "forward 8",
      "up 3",
      "down 8",
      "forward 2",
    )
    val answer = Part2.solve(commands)
    answer shouldEqual 900
  }

}
