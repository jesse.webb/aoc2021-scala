package jessewebb.aoc2021.day01

object Part2 {

  def solve(measurements: Seq[Int]): Int = {
    measurements
      .sliding(3)
      .sliding(2)
      .count { case Seq(window1, window2) => window2.sum > window1.sum}
  }

}
