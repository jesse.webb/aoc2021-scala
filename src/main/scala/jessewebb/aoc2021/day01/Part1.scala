package jessewebb.aoc2021.day01

object Part1 {

  def solve(measurements: Seq[Int]): Int = {
    measurements
      .sliding(2)
      .count { case Seq(x, y) => y > x }
  }

}
