package jessewebb.aoc2021.day02

object Part2 {

  def solve(commands: Seq[String]): Int = {
    val (horizontalPosition, depth, aim) = commands.foldLeft((0, 0, 0)) { (acc, str) =>
      str.split(' ') match {
        case Array("down", x) => (acc._1, acc._2, acc._3 + x.toInt)
        case Array("up", x) => (acc._1, acc._2, acc._3 - x.toInt)
        case Array("forward", x) => (acc._1 + x.toInt, acc._2 + (acc._3 * x.toInt), acc._3)
      }
    }
    horizontalPosition * depth
  }

}
