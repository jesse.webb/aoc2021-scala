package jessewebb.aoc2021.day02

object Part1 {

  def solve(commands: Seq[String]): Int = {
    val (horizontalPosition, depth) = commands.foldLeft((0, 0)) { (acc, str) =>
      str.split(' ') match {
        case Array("forward", x) => (acc._1 + x.toInt, acc._2)
        case Array("down", x) => (acc._1, acc._2 + x.toInt)
        case Array("up", x) => (acc._1, acc._2 - x.toInt)
      }
    }
    horizontalPosition * depth
  }

}
