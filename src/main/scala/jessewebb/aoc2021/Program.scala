package jessewebb.aoc2021

object Program extends App {

  def solveDay01(): Unit = {
    println("Day 01\n======")
    import jessewebb.aoc2021.day01._
    val input = PuzzleInput.Day01
    val measurements = input.split('\n').map(_.trim.toInt).toList
    val answer1 = Part1.solve(measurements)
    println(s"Part 1: $answer1")
    val answer2 = Part2.solve(measurements)
    println(s"Part 2: $answer2")
  }

  def solveDay02(): Unit = {
    println("Day 02\n======")
    import jessewebb.aoc2021.day02._
    val input = PuzzleInput.Day02
    val commands = input.split('\n').map(_.trim).toList
    val answer1 = Part1.solve(commands)
    println(s"Part 1: $answer1")
    val answer2 = Part2.solve(commands)
    println(s"Part 2: $answer2")
  }

  def solveDay03(): Unit = {
    println("Day 03\n======")
    import jessewebb.aoc2021.day03._
    val input = PuzzleInput.Day03
    val binaryNumbers = input.split('\n').map(_.trim).toList
    val answer1 = Part1.solve(binaryNumbers)
    println(s"Part 1: $answer1")
    val answer2 = Part2.solve(binaryNumbers)
    println(s"Part 2: $answer2")
  }

  // solveDay01()
  // solveDay02()
  solveDay03()

}
