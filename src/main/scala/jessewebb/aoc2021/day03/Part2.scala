package jessewebb.aoc2021.day03

import scala.annotation.tailrec

object Part2 {

  def solve(binaryNumbers: Seq[String]): Int = {
    val oxygenGeneratorRatingStr = oxygenGeneratorRatingBitCriteria(binaryNumbers)
    val oxygenGeneratorRating = Integer.parseUnsignedInt(oxygenGeneratorRatingStr, 2)
    val co2ScrubberRatingStr = co2ScrubberRatingBitCriteria(binaryNumbers)
    val co2ScrubberRating = Integer.parseUnsignedInt(co2ScrubberRatingStr, 2)

    val lifeSupportRating = oxygenGeneratorRating * co2ScrubberRating
    lifeSupportRating
  }

  @tailrec
  private def oxygenGeneratorRatingBitCriteria(binaryNumbers: Seq[String], index: Int = 0): String = {
    if (binaryNumbers.length == 1)
      binaryNumbers.head
    else {
      val bitCountsForColumn = binaryNumbers
        .map(_(index).asDigit)
        .groupBy(identity)
        .view.mapValues(_.length).toMap
      val bitToKeep = if (bitCountsForColumn(0) == bitCountsForColumn(1)) 1 else bitCountsForColumn.maxBy(_._2)._1
      val binaryNumbersToKeep = binaryNumbers.filter(_(index).asDigit == bitToKeep)
      oxygenGeneratorRatingBitCriteria(binaryNumbersToKeep, index + 1)
    }
  }

  @tailrec
  private def co2ScrubberRatingBitCriteria(binaryNumbers: Seq[String], index: Int = 0): String = {
    if (binaryNumbers.length == 1)
      binaryNumbers.head
    else {
      val bitCountsForColumn = binaryNumbers
        .map(_(index).asDigit)
        .groupBy(identity)
        .view.mapValues(_.length).toMap
      val bitToKeep = if (bitCountsForColumn(0) == bitCountsForColumn(1)) 0 else bitCountsForColumn.minBy(_._2)._1
      val binaryNumbersToKeep = binaryNumbers.filter(_(index).asDigit == bitToKeep)
      co2ScrubberRatingBitCriteria(binaryNumbersToKeep, index + 1)
    }
  }

}
