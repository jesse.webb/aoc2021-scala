package jessewebb.aoc2021.day03

object Part1 {

  def solve(binaryNumbers: Seq[String]): Int = {
    val bitCountsForColumns = binaryNumbers
      .map(_.map(_.asDigit))
      .transpose
      .map(_.groupBy(identity).view.mapValues(_.length).toMap)
    val gammaRateStr = bitCountsForColumns.map(_.maxBy(_._2)._1.toBinaryString).mkString
    val gammaRate = Integer.parseUnsignedInt(gammaRateStr, 2)
    val epsilonRateStr = bitCountsForColumns.map(_.minBy(_._2)._1.toBinaryString).mkString
    val epsilonRate = Integer.parseUnsignedInt(epsilonRateStr, 2)

    val powerConsumption = gammaRate * epsilonRate
    powerConsumption
  }

}
